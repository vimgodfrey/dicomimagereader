/*========================================================

Program: DICOM Image Viewer

Michael Godfrey 
Senior Design Project
BS Software Engineering
Fall 2015

========================================================*/
 
#include "stdafx.h"
#include "ReadDICOM.h" 
#include <vtkSmartPointer.h>
#include <vtkImageViewer.h>
#include <vtkDICOMImageReader.h>
#include <vtkRenderWindow.h>
#include <vtkWin32OpenGLRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkImageResize.h>
#include <vtkImageData.h>
#include <vtkImageReslice.h>
#include <vtkMatrix4x4.h>

// Draw Line
#include <ObjIdl.h>
#include <gdiplus.h>
using namespace Gdiplus;
#pragma comment (lib, "Gdiplus.lib")


#define MAX_LOADSTRING	100
#define AXIAL			401
#define CORONAL			402
#define SAGITTAL		403

// Global Variables:
// PARENT WINDOW
HINSTANCE	hInst;								// current instance
HWND		hWndMain;
int			m_nCmdShow;							// (minimized, maximized, etc.)
WCHAR		szTitle[MAX_LOADSTRING];            // The title bar text
WCHAR		szWindowClass[MAX_LOADSTRING];      // the main window class name

ATOM MyRegisterClass(HINSTANCE hInstance);
BOOL InitInstance(HINSTANCE, int);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK About(HWND, UINT, WPARAM, LPARAM);

// VTK Objects used to display DICOM Image
vtkSmartPointer<vtkDICOMImageReader> dicomImageReader;
int iPosX;
int iPosY;
BOOL mDown; 
BOOL g_dicomImageLoaded;

// Win32 Main Window 
const wchar_t		g_szDICOMWindowClass[] = L"DICOMImageWindow";
const wchar_t		g_szDICOMWindowTitle[] = L"DICOMImageTitle";
HWND				hWndVtk;
ATOM RegisterDICOMImageHolderWindow(HINSTANCE);
LRESULT CALLBACK WndProcVTKImage(HWND, UINT, WPARAM, LPARAM); 

//DICOM Image Holder Main Window 
int g_aSlice;
vtkSmartPointer<vtkImageReslice> imageReslice;
vtkSmartPointer<vtkMatrix4x4> resliceAxes;
vtkSmartPointer<vtkImageResize> imageResize;
vtkSmartPointer<vtkImageViewer> imageViewer;
vtkSmartPointer<vtkWin32OpenGLRenderWindow> win32OpenGLRenderWindow;
VOID OpenDICOMImage(LPSTR imgDir);
VOID ResizeDicomImage(); 
VOID UpdateDICOMWidthCenter(int cPosX, int cPosY, vtkSmartPointer<vtkImageViewer>);
VOID DisplayDicomText(HDC);
VOID DrawLinesForWindow(HWND, HDC, double hLine, double vLine, int view);

// Win32 TopRightMini Window
const wchar_t		g_szTopRightMiniClass[] = L"TopRightMiniClass";
const wchar_t		g_szTopRightMiniTitle[] = L"TopRightMiniTitle";
HWND hWndRMT;
ATOM RegisterTopRightMiniWindow(HINSTANCE);
LRESULT CALLBACK WndProcTopRightMini(HWND, UINT, WPARAM, LPARAM);

// Top Right Mini Window
int g_cSlice;
vtkSmartPointer<vtkImageReslice> rt_imageReslice;
vtkSmartPointer<vtkMatrix4x4> rt_resliceAxes;
vtkSmartPointer<vtkImageResize> rt_imageResize;
vtkSmartPointer<vtkImageViewer> rt_imageViewer;
vtkSmartPointer<vtkWin32OpenGLRenderWindow> rt_win32OpenGLRenderWindow; 
VOID ResizeTopRightMiniDicomImage();
VOID DisplayTopRightMiniText(HDC hdc);
 
///////////////////////////////////////////////////
// Win32 BottomRightMini Window
const wchar_t		g_szBottomRightMiniClass[] = L"BottomRightMiniClass";
const wchar_t		g_szBottomRightMiniTitle[] = L"BottomRightMiniTitle";
HWND hWndBRM;
ATOM RegisterBottomRightMiniWindow(HINSTANCE);
LRESULT CALLBACK WndProcBottomRightMini(HWND, UINT, WPARAM, LPARAM); 

// Bottom Right Mini Window
int g_sSlice;
vtkSmartPointer<vtkImageReslice> rb_imageReslice;
vtkSmartPointer<vtkMatrix4x4> rb_resliceAxes;
vtkSmartPointer<vtkImageResize> rb_imageResize;
vtkSmartPointer<vtkImageViewer> rb_imageViewer;
vtkSmartPointer<vtkWin32OpenGLRenderWindow> rb_win32OpenGLRenderWindow; 
VOID ResizeBottomRightMiniDicomImage();
VOID DisplayBottomRightMiniText(HDC hdc);
///////////////////////////////////////////////////

// Open File Browser
BOOL g_singleFileOpen;
VOID OpenFileDialog();

// Line
double g_line[3];
VOID UpdateLinePos();


int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow) {

	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.

	///////////////////////////////////////////////////
	// Line Drawing
	GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR			gdiplusToken; 
	// Init GDI+
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
	///////////////////////////////////////////////////

	// Initialize global strings
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	//const wchar_t		g_szDICOMWindowTitle[] = L"DICOMImageTitle";
	LoadStringW(hInstance, IDC_WIN32VTK1, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);


	////RegisterDICOMImageHolderWindow(hInstance);
	if (!RegisterDICOMImageHolderWindow(hInstance))
		return FALSE;

	if (!RegisterTopRightMiniWindow(hInstance))
		return FALSE;

	if (!RegisterBottomRightMiniWindow(hInstance))
		return FALSE;

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
		return FALSE;

	// Used to register keyboard shortcuts, defined in Resources
	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_WIN32VTK1));

	MSG msg;
	// Main message loop:
	while (GetMessage(&msg, nullptr, 0, 0)) {
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	// Free memory here?
	///////////////////////////////////////////////////
	GdiplusShutdown(gdiplusToken);
	///////////////////////////////////////////////////

	return (int)msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance) {
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_WIN32VTK1));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_WIN32VTK1);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

// hInstance: A handle to the instance that contains the window prcedure for the class
ATOM RegisterDICOMImageHolderWindow(HINSTANCE hInstance) {
	WNDCLASSEXW wdicom;
	wdicom.cbSize = sizeof(WNDCLASSEX);

	wdicom.style = CS_HREDRAW | CS_VREDRAW;
	wdicom.lpfnWndProc = WndProcVTKImage;					// My own event handler
	wdicom.cbClsExtra = 0;
	wdicom.cbWndExtra = 0;
	wdicom.hInstance = hInstance;
	wdicom.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wdicom.hCursor = LoadCursor(NULL, IDC_ARROW);
	wdicom.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wdicom.lpszMenuName = NULL;
	wdicom.lpszClassName = g_szDICOMWindowClass;		// with its own class name.
	wdicom.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	return RegisterClassExW(&wdicom);
}


ATOM RegisterTopRightMiniWindow(HINSTANCE hInstance) {
	WNDCLASSEXW wRMini;
	wRMini.cbSize = sizeof(WNDCLASSEX);
 
	wRMini.style = CS_HREDRAW | CS_VREDRAW;
	wRMini.lpfnWndProc = WndProcTopRightMini;
	wRMini.cbClsExtra = 0;
	wRMini.cbWndExtra = 0;
	wRMini.hInstance = hInstance;
	wRMini.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wRMini.hCursor = LoadCursor(NULL, IDC_ARROW);
	//wRMini.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);	// add a color here
	wRMini.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);	// add a color here
	wRMini.lpszMenuName = NULL;
	wRMini.lpszClassName = g_szTopRightMiniClass;
	wRMini.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	return RegisterClassExW(&wRMini);
}

ATOM RegisterBottomRightMiniWindow(HINSTANCE hInstance) {
	WNDCLASSEXW wBRMini;
	wBRMini.cbSize = sizeof(WNDCLASSEX);
 
	wBRMini.style = CS_HREDRAW | CS_VREDRAW;
	wBRMini.lpfnWndProc = WndProcBottomRightMini;
	wBRMini.cbClsExtra = 0;
	wBRMini.cbWndExtra = 0;
	wBRMini.hInstance = hInstance;
	wBRMini.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wBRMini.hCursor = LoadCursor(NULL, IDC_ARROW);
	//wBRMini.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);	// add a color here
	wBRMini.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);	// add a color here
	wBRMini.lpszMenuName = NULL;
	wBRMini.lpszClassName = g_szBottomRightMiniClass;
	wBRMini.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	return RegisterClassExW(&wBRMini);
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow) {
	hInst = hInstance; // Store instance handle in our global variable
	m_nCmdShow = nCmdShow;

	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
		return FALSE;

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}
 

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
	case WM_COMMAND: {
		int wmId = LOWORD(wParam);
		// MENU
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case ID_FILE_OPEN:
			g_singleFileOpen = TRUE;
			g_dicomImageLoaded = FALSE;
			OpenFileDialog();
			break;
		case ID_FILE_OPENSERIES:
			g_singleFileOpen = FALSE;
			g_dicomImageLoaded = FALSE;
			OpenFileDialog();
			break;
		default:
			return DefWindowProcW(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_PAINT: {
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		EndPaint(hWnd, &ps);

		// Resize the child win32 window 
		LPRECT lpRectMain = (LPRECT)malloc(sizeof(RECT));
		GetWindowRect(hWndMain, lpRectMain);

		SetWindowPos(hWndVtk, NULL, 0, 0, 
			(lpRectMain->right - lpRectMain->left)/2, 
			(lpRectMain->bottom - lpRectMain->top), NULL); 

		SetWindowPos(hWndRMT, NULL, 
			(lpRectMain->right - lpRectMain->left)/2,
			0,
			(lpRectMain->right - lpRectMain->left)/2, 
			(lpRectMain->bottom - lpRectMain->top - SM_CYMENUSIZE)/2,
			NULL);

		SetWindowPos(hWndBRM, NULL, 
			(lpRectMain->right - lpRectMain->left)/2,
			(lpRectMain->bottom - lpRectMain->top - SM_CYMENUSIZE)/2,
			(lpRectMain->right - lpRectMain->left)/2, 
			(lpRectMain->bottom - lpRectMain->top)/2, NULL);

		// Resize the VTK image if present
		if (g_dicomImageLoaded) {
			ResizeDicomImage();
			if (!g_singleFileOpen) { 
				ResizeTopRightMiniDicomImage();
				ResizeBottomRightMiniDicomImage();
			}
		} 
	}
	break;
	case WM_LBUTTONDOWN:
		OutputDebugStringW(L"WM_LBUTTONDOWN\n");
		break;
	case WM_CREATE: {
		hWndMain = hWnd;
		LPRECT lpRectMain = (LPRECT)malloc(sizeof(RECT));
		//GetWindowRect(hWndMain, lpRectMain);
		GetClientRect(hWndMain, lpRectMain);
		////////////////////////////////////////////////////
		// Create the Window that will hold the DICOM Image
		HWND hWndDicom = CreateWindowW( g_szDICOMWindowClass, g_szDICOMWindowTitle, 
			WS_CHILD | WS_CLIPSIBLINGS,
			0, 0,
			(lpRectMain->right - lpRectMain->left)/2,
			(lpRectMain->bottom - lpRectMain->top),
			hWnd, NULL, hInst, NULL );

		if (!hWndDicom)
			return FALSE;
		ShowWindow(hWndDicom, m_nCmdShow);
		UpdateWindow(hWndDicom); 
		//////////////////////////////////////////////////// 
		// Top Right Mini
		HWND hWndRMTini = CreateWindowW( g_szTopRightMiniClass, g_szTopRightMiniTitle, 
			WS_CHILD | WS_CLIPSIBLINGS,// | WS_BORDER,
			(lpRectMain->right - lpRectMain->left)/2,
			0,
			(lpRectMain->right - lpRectMain->left)/2, 
			(lpRectMain->bottom - lpRectMain->top)/2,
			hWnd, NULL, hInst, NULL ); 
		if (!hWndRMTini)
			return FALSE;
		ShowWindow(hWndRMTini, m_nCmdShow);
		UpdateWindow(hWndRMTini);

		//////////////////////////////////////////////////// 
		// Bottom Right Mini
		HWND hWndBRMini = CreateWindowW( g_szBottomRightMiniClass, g_szBottomRightMiniTitle, 
			WS_CHILD | WS_CLIPSIBLINGS,// | WS_BORDER,
			(lpRectMain->right - lpRectMain->left)/2,
			(lpRectMain->bottom - lpRectMain->top)/2,
			(lpRectMain->right - lpRectMain->left)/2, 
			(lpRectMain->bottom - lpRectMain->top)/2,
			hWnd, NULL, hInst, NULL ); 
		if (!hWndBRMini)
			return FALSE;
		ShowWindow(hWndBRMini, m_nCmdShow);
		UpdateWindow(hWndBRMini);
		break;
	}
	case WM_DESTROY:
		// TODO release all of the vtk objects that are in use...
		// Duno, these throw an access violation, but they debug fine?
		//if (g_dicomImageLoaded) { 
		//	dicomImageReader->Delete();
		//	imageResize->Delete();
		//	imageViewer->Delete();
		//	win32OpenGLRenderWindow->Delete();
		//	imageReslice->Delete();
		//	if (!g_singleFileOpen) {
		//		rt_imageResize->Delete();
		//		rt_imageViewer->Delete();
		//		rt_win32OpenGLRenderWindow->Delete();
		//		rt_imageReslice->Delete(); 
		//		rb_imageResize->Delete();
		//		rb_imageViewer->Delete();
		//		rb_win32OpenGLRenderWindow->Delete();
		//		rb_imageReslice->Delete(); 
		//	} 
		//}
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProcW(hWnd, message, wParam, lParam);
	}
	return 0;
}


INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam) {
	//OutputDebugStringW(L"CALLBACK About");
	UNREFERENCED_PARAMETER(lParam);
	switch (message) {
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND: {
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) {
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	}
	return (INT_PTR)FALSE;
}


LRESULT CALLBACK WndProcVTKImage(HWND hVtk, UINT message, WPARAM wParam, LPARAM lParam) {
	UNREFERENCED_PARAMETER(lParam);
	HDC			hdc;
	PAINTSTRUCT ps; 
	switch (message) {
	case WM_CREATE:
		hWndVtk = hVtk;
		g_dicomImageLoaded = false;
		break;
	case WM_PAINT:
		hdc = BeginPaint(hVtk, &ps);

		if (g_dicomImageLoaded) {
			win32OpenGLRenderWindow->Render();
			DisplayDicomText(hdc);

			if (!g_singleFileOpen) {
				UpdateLinePos();
				DrawLinesForWindow(hVtk, hdc, g_line[0], g_line[1], AXIAL);
			}
		}

		EndPaint(hVtk, &ps);
		ReleaseDC(hVtk, hdc);
		break;
	case WM_LBUTTONDOWN:
		if (g_dicomImageLoaded) {
			iPosX = LOWORD(lParam);
			iPosY = HIWORD(lParam);
			mDown = true;
		}
		break;
	case WM_MOUSEMOVE:
		if (mDown) {
			int cPosX = LOWORD(lParam);
			int cPosY = HIWORD(lParam);
			UpdateDICOMWidthCenter(cPosX, cPosY, imageViewer);
			InvalidateRect(hWndVtk, NULL, FALSE);
		}
		break;
	case WM_LBUTTONUP:
		mDown = false;
		break;
	case WM_MOUSELEAVE:
		mDown = false;
		break;
	case WM_MOUSEWHEEL:
		if (g_dicomImageLoaded) {
			if(!g_singleFileOpen) {
				int distance = GET_WHEEL_DELTA_WPARAM(wParam);

				if (distance > 0) {

					int extent[6];
					double spacing[3];
					double origin[3]; 
					dicomImageReader->GetDataExtent(extent);
					dicomImageReader->GetOutput()->GetSpacing(spacing);
					dicomImageReader->GetOutput()->GetOrigin(origin);
					double maxSlice = (extent[5] - extent[4]) * spacing[2]; 

					if (g_aSlice + 1 < maxSlice) {
						g_aSlice = g_aSlice + 1; 
						resliceAxes->SetElement(2, 3, g_aSlice);
						imageReslice->SetResliceAxes(resliceAxes);
					}
				} else {
					double origin[3];
					dicomImageReader->GetOutput()->GetOrigin(origin);
					if (g_aSlice - 1 > origin[2]) {
						g_aSlice = g_aSlice - 1; 
						resliceAxes->SetElement(2, 3, g_aSlice);
						imageReslice->SetResliceAxes(resliceAxes);
					}
				} 
				// All three, we need to update the line.
				InvalidateRect(hWndVtk, NULL, FALSE);
				InvalidateRect(hWndRMT, NULL, FALSE);
				InvalidateRect(hWndBRM, NULL, FALSE);
			}
		}
		break;
	default:
		return DefWindowProcW(hVtk, message, wParam, lParam);
	}
	return 0;
}


LRESULT CALLBACK WndProcTopRightMini(HWND hWndRMTini, UINT message, WPARAM wParam, LPARAM lParam) {
	UNREFERENCED_PARAMETER(lParam);
	HDC			hdc;
	PAINTSTRUCT ps;
	switch (message) {
	case WM_CREATE:
		hWndRMT = hWndRMTini;
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWndRMTini, &ps);
		if (g_dicomImageLoaded && !g_singleFileOpen) {
			rt_win32OpenGLRenderWindow->Render();
			DisplayTopRightMiniText(hdc);
			UpdateLinePos();
			DrawLinesForWindow(hWndRMTini, hdc, g_line[2], g_line[1], CORONAL);
		}
		EndPaint(hWndRMTini, &ps);
		ReleaseDC(hWndRMTini, hdc);
		break;
	case WM_LBUTTONDOWN:
		if (g_dicomImageLoaded && !g_singleFileOpen) {
			iPosX = LOWORD(lParam);
			iPosY = HIWORD(lParam);
			mDown = true;
		}
		break;
	case WM_MOUSEMOVE:
		if (mDown) {
			int cPosX = LOWORD(lParam);
			int cPosY = HIWORD(lParam);
			UpdateDICOMWidthCenter(cPosX, cPosY, rt_imageViewer);
			InvalidateRect(hWndRMTini, NULL, FALSE);
		}
		break;
	case WM_LBUTTONUP:
		mDown = false;
		break;
	case WM_MOUSELEAVE:
		mDown = false;
		break;
	case WM_MOUSEWHEEL:
		// CORONAL
		if (g_dicomImageLoaded && !g_singleFileOpen) {
			if(!g_singleFileOpen) {
				// TODO: move me to my own function
				int distance = GET_WHEEL_DELTA_WPARAM(wParam); 
				if (distance > 0) { 
					int extent[6];
					double spacing[3];
					double origin[3]; 
					dicomImageReader->GetDataExtent(extent);
					dicomImageReader->GetOutput()->GetSpacing(spacing);
					dicomImageReader->GetOutput()->GetOrigin(origin);
					double maxSlice = (extent[1] - extent[0]) * spacing[0]; 

					if (g_cSlice + 1 < maxSlice) {
						g_cSlice = g_cSlice + 1; 
						rt_resliceAxes->SetElement(1, 3, g_cSlice);
						rt_imageReslice->SetResliceAxes(rt_resliceAxes); 
					}
				} else {
					double origin[3];
					dicomImageReader->GetOutput()->GetOrigin(origin);
					if (g_cSlice - 1 > origin[0]) {
						g_cSlice = g_cSlice - 1; 
						rt_resliceAxes->SetElement(1, 3, g_cSlice);
						rt_imageReslice->SetResliceAxes(rt_resliceAxes);
					}
				} 
				InvalidateRect(hWndVtk, NULL, FALSE);
				InvalidateRect(hWndRMTini, NULL, FALSE);
				InvalidateRect(hWndBRM, NULL, FALSE);
			}
		}
		break;
	default:
		return DefWindowProcW(hWndRMTini, message, wParam, lParam);
	}
	return 0;
}

 
LRESULT CALLBACK WndProcBottomRightMini(HWND hWndBRMini, UINT message, WPARAM wParam, LPARAM lParam) {
	UNREFERENCED_PARAMETER(lParam);
	HDC			hdc;
	PAINTSTRUCT ps;
	switch (message) {
	case WM_CREATE:
		hWndBRM = hWndBRMini;
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWndBRMini, &ps);

		if (g_dicomImageLoaded && !g_singleFileOpen) {
			rb_win32OpenGLRenderWindow->Render();
			DisplayBottomRightMiniText(hdc);
			UpdateLinePos();
			DrawLinesForWindow(hWndBRMini, hdc, g_line[2], g_line[0], SAGITTAL);
		} 

		EndPaint(hWndBRMini, &ps);
		ReleaseDC(hWndBRMini, hdc);
		break;
	case WM_LBUTTONDOWN:
		if (g_dicomImageLoaded && !g_singleFileOpen) {
			iPosX = LOWORD(lParam);
			iPosY = HIWORD(lParam);
			mDown = true;
		}
		break;
	case WM_MOUSEMOVE:
		if (mDown) {
			int cPosX = LOWORD(lParam);
			int cPosY = HIWORD(lParam);
			UpdateDICOMWidthCenter(cPosX, cPosY, rb_imageViewer);
			InvalidateRect(hWndBRMini, NULL, FALSE);
		}
		break;
	case WM_LBUTTONUP:
		mDown = false;
		break;
	case WM_MOUSELEAVE:
		mDown = false;
		break;
	case WM_MOUSEWHEEL:
		// CORONAL
		if (g_dicomImageLoaded && !g_singleFileOpen) {
			if(!g_singleFileOpen) {
				int distance = GET_WHEEL_DELTA_WPARAM(wParam); 
				if (distance > 0) { 
					int extent[6];
					double spacing[3];
					double origin[3]; 
					dicomImageReader->GetDataExtent(extent);
					dicomImageReader->GetOutput()->GetSpacing(spacing);
					dicomImageReader->GetOutput()->GetOrigin(origin);
					double maxSlice = (extent[3] - extent[2]) * spacing[1]; 

					if (g_sSlice + 1 < maxSlice) {
						g_sSlice = g_sSlice + 1;
						rb_resliceAxes->SetElement(0, 3, g_sSlice);
						rb_imageReslice->SetResliceAxes(rb_resliceAxes);
					}
				} else {
					double origin[3];
					dicomImageReader->GetOutput()->GetOrigin(origin);
					if (g_sSlice - 1 > origin[1]) {
						g_sSlice = g_sSlice - 1; 
						rb_resliceAxes->SetElement(0, 3, g_sSlice);
						rb_imageReslice->SetResliceAxes(rb_resliceAxes);
					}
				} 
				InvalidateRect(hWndVtk, NULL, FALSE);
				InvalidateRect(hWndRMT, NULL, FALSE);
				InvalidateRect(hWndBRMini, NULL, FALSE);
			}
		}
		break;
	default:
		return DefWindowProcW(hWndBRMini, message, wParam, lParam);
	}
	return 0;
}

VOID UpdateDICOMWidthCenter(int cPosX, int cPosY, vtkSmartPointer<vtkImageViewer> iv) {
	//LEVEL
	int dX = iPosX - cPosX;
	int newLevel = iv->GetColorLevel() + (dX * 4);
	newLevel = (newLevel < -2000) ? -2000 : newLevel;
	newLevel = (newLevel > 2000) ? 2000 : newLevel;
	iv->SetColorLevel(newLevel);
	iPosX = cPosX; 
	// WIDTH
	int dY = iPosY - cPosY; 
	int newWindow = iv->GetColorWindow() + (dY * 10);
	newWindow = (newWindow < 10) ? 10 : newWindow;
	newWindow = (newWindow > 5000) ? 5000 : newWindow; 
	iv->SetColorWindow(newWindow);
	iPosY = cPosY;
}


VOID OpenDICOMImage(LPSTR path) {

	dicomImageReader = vtkSmartPointer<vtkDICOMImageReader>::New();
	imageResize = vtkSmartPointer<vtkImageResize>::New();
	imageViewer = vtkSmartPointer<vtkImageViewer>::New();
	win32OpenGLRenderWindow = vtkSmartPointer<vtkWin32OpenGLRenderWindow>::New();
	imageReslice = vtkSmartPointer<vtkImageReslice>::New();

	imageReslice->SetInputConnection(dicomImageReader->GetOutputPort());
	imageResize->SetInputConnection(imageReslice->GetOutputPort());
	imageViewer->SetInputConnection(imageResize->GetOutputPort());
	win32OpenGLRenderWindow->AddRenderer(imageViewer->GetRenderer());
	win32OpenGLRenderWindow->SetWindowId(hWndVtk);

	// Single File
	if (g_singleFileOpen) {
		if (dicomImageReader->CanReadFile(path)) {
			dicomImageReader->SetFileName(path);
			dicomImageReader->Update();

			//Resize Win32 Holder to fill screen
			LPRECT lpRectMain = (LPRECT)malloc(sizeof(RECT));
			GetWindowRect(hWndMain, lpRectMain);
			SetWindowPos(hWndVtk, NULL, 0, 0,
			(lpRectMain->right - lpRectMain->left), 
			(lpRectMain->bottom - lpRectMain->top), NULL); 

			ResizeDicomImage();
			g_dicomImageLoaded = true;
			InvalidateRect(hWndVtk, NULL, FALSE);
		}
	}
	// Directory ie. Series
	else {
		dicomImageReader->SetDirectoryName(path);
		dicomImageReader->Update(); 

		//Resize Win32 Holder to fill half screen
		LPRECT lpRectMain = (LPRECT)malloc(sizeof(RECT));
		GetWindowRect(hWndMain, lpRectMain);
		SetWindowPos(hWndVtk, NULL, 0, 0,
		(lpRectMain->right - lpRectMain->left)/2, 
		(lpRectMain->bottom - lpRectMain->top)/2, NULL); 
		///////////////////////////////////////////////////////////////////////////////////
		// SLICE AND GRAB
		// Calculate the center of the volume
		int extent[6];
		double spacing[3];
		double origin[3];
		
		dicomImageReader->GetDataExtent(extent);
		dicomImageReader->GetOutput()->GetSpacing(spacing);
		dicomImageReader->GetOutput()->GetOrigin(origin);
		
		double center[3];
		// x,y,z plane centers
		center[0] = origin[0] + spacing[0] * 0.5 * (extent[0] + extent[1]);
		center[1] = origin[1] + spacing[1] * 0.5 * (extent[2] + extent[3]);
		center[2] = origin[2] + spacing[2] * 0.5 * (extent[4] + extent[5]);
 
		///////////////////////////////////////////////////////////////////////////////////
		// AXIAL 
		///////////////////////////////////////////////////////////////////////////////////
		// USE ImageReslice to resize stack 
		// Matrices for axial, coronal, sagittal, oblique view orientations
	    static double axialElements[16] = {
		         1, 0, 0, 0,
		         0, 1, 0, 0,
		         0, 0, 1, 0,
		         0, 0, 0, 1 };
		
	    // Set the slice orientation
		resliceAxes = vtkSmartPointer<vtkMatrix4x4>::New();
		resliceAxes->DeepCopy(axialElements); 

		g_aSlice = center[2];
		resliceAxes->SetElement(0, 3, center[0]);
		resliceAxes->SetElement(1, 3, center[1]);
		resliceAxes->SetElement(2, 3, g_aSlice); 

		imageReslice->SetResliceAxes(resliceAxes); 
		// gives a 2D slice specified by the 4x4 matrix we contructed
		imageReslice->SetOutputDimensionality(2);
		imageReslice->SetInterpolationModeToLinear(); 
		///////////////////////////////////////////////////////////////////////////////////
		ResizeDicomImage(); 
		g_dicomImageLoaded = true;
		InvalidateRect(hWndVtk, NULL, FALSE);
		///////////////////////////////////////////////////////////////////////////////////
		// CORONAL 
		///////////////////////////////////////////////////////////////////////////////////
		rt_imageResize = vtkSmartPointer<vtkImageResize>::New();
		rt_imageViewer = vtkSmartPointer<vtkImageViewer>::New();
		rt_win32OpenGLRenderWindow = vtkSmartPointer<vtkWin32OpenGLRenderWindow>::New();
		rt_imageReslice = vtkSmartPointer<vtkImageReslice>::New(); 

		rt_imageReslice->SetInputConnection(dicomImageReader->GetOutputPort());
		rt_imageResize->SetInputConnection(rt_imageReslice->GetOutputPort());
		rt_imageViewer->SetInputConnection(rt_imageResize->GetOutputPort());
		rt_win32OpenGLRenderWindow->AddRenderer(rt_imageViewer->GetRenderer());
		rt_win32OpenGLRenderWindow->SetWindowId(hWndRMT);

		static double coronalElements[16] = {
		         1, 0, 0, 0,
		         0, 0, 1, 0,
		         0,-1, 0, 0,
		         0, 0, 0, 1 };

		rt_resliceAxes = vtkSmartPointer<vtkMatrix4x4>::New();
		rt_resliceAxes->DeepCopy(coronalElements); 

		g_cSlice = center[1];

		rt_resliceAxes->SetElement(0, 3, center[0]);
		rt_resliceAxes->SetElement(1, 3, g_cSlice);
		rt_resliceAxes->SetElement(2, 3, center[2]);

		rt_imageReslice->SetResliceAxes(rt_resliceAxes); 
		// gives a 2D slice specified by the 4x4 matrix we contructed
		rt_imageReslice->SetOutputDimensionality(2);
		rt_imageReslice->SetInterpolationModeToLinear(); 
		///////////////////////////////////////////////////////////////////////////////////
		ResizeTopRightMiniDicomImage(); 
		InvalidateRect(hWndRMT, NULL, FALSE); 
		///////////////////////////////////////////////////////////////////////////////////
		// SAGITTAL 
		/////////////////////////////////////////////////////////////////////////////////// 
		rb_imageResize = vtkSmartPointer<vtkImageResize>::New();
		rb_imageViewer = vtkSmartPointer<vtkImageViewer>::New();
		rb_win32OpenGLRenderWindow = vtkSmartPointer<vtkWin32OpenGLRenderWindow>::New();
		rb_imageReslice = vtkSmartPointer<vtkImageReslice>::New(); 

		rb_imageReslice->SetInputConnection(dicomImageReader->GetOutputPort());
		rb_imageResize->SetInputConnection(rb_imageReslice->GetOutputPort());
		rb_imageViewer->SetInputConnection(rb_imageResize->GetOutputPort());
		rb_win32OpenGLRenderWindow->AddRenderer(rb_imageViewer->GetRenderer());
		rb_win32OpenGLRenderWindow->SetWindowId(hWndBRM);


		static double sagittalElements[16] = {
		         0, 0,-1, 0,
		         1, 0, 0, 0,
		         0,-1, 0, 0,
		         0, 0, 0, 1 };

		rb_resliceAxes = vtkSmartPointer<vtkMatrix4x4>::New();
		rb_resliceAxes->DeepCopy(sagittalElements); 

		g_sSlice = center[0];

		//rb_resliceAxes->SetElement(0, 3, center[0]);
		rb_resliceAxes->SetElement(0, 3, g_sSlice);
		rb_resliceAxes->SetElement(1, 3, center[1]);
		rb_resliceAxes->SetElement(2, 3, center[2]);

		rb_imageReslice->SetResliceAxes(rb_resliceAxes); 
		// gives a 2D slice specified by the 4x4 matrix we contructed
		rb_imageReslice->SetOutputDimensionality(2);
		rb_imageReslice->SetInterpolationModeToLinear(); 
		///////////////////////////////////////////////////////////////////////////////////
		ResizeBottomRightMiniDicomImage(); 
		InvalidateRect(hWndBRM, NULL, FALSE);
	} 
}

VOID ResizeTopRightMiniDicomImage() { 
	// Get size of Win32 TopRightMiniHolderWindow
	LPRECT lpRectTRMini = (LPRECT)malloc(sizeof(RECT));
	GetWindowRect(hWndRMT, lpRectTRMini);
	int wWidth = (lpRectTRMini->right - lpRectTRMini->left);
	int wHeight = (lpRectTRMini->bottom - lpRectTRMini->top);

	int extent[6];
	dicomImageReader->GetDataExtent(extent);
	int dims[] = {
		extent[0] + extent[1],
		extent[2] + extent[3],
		extent[4] + extent[5]
	};

	//Limit to prevent stretching of images
	int dWidth = dims[0] - wWidth;
	int dHeight = dims[1] - wHeight;
	float resize;
	// Find smallest width/height difference and use it as the scaling factor;
	if (dWidth > dHeight) {
		resize = (float)wWidth / dims[0];
	} else {
		resize = (float)wHeight / dims[1];
	} 
	rt_imageResize->SetOutputDimensions( dims[0] * resize, dims[1] * resize, 1);
}


 VOID ResizeBottomRightMiniDicomImage() { 
	// Get size of Win32 TopRightMiniHolderWindow
	LPRECT lpRectBRMini = (LPRECT)malloc(sizeof(RECT));
	GetWindowRect(hWndBRM, lpRectBRMini);
	int wWidth = (lpRectBRMini->right - lpRectBRMini->left);
	int wHeight = (lpRectBRMini->bottom - lpRectBRMini->top);

	int extent[6];
	dicomImageReader->GetDataExtent(extent);
	int dims[] = {
		extent[0] + extent[1],
		extent[2] + extent[3],
		extent[4] + extent[5]
	};

	//Limit to prevent stretching of images
	int dWidth = dims[0] - wWidth;
	int dHeight = dims[1] - wHeight;
	float resize;
	// Find smallest width/height difference and use it as the scaling factor;
	if (dWidth > dHeight) {
		resize = (float)wWidth / dims[0];
	} else {
		resize = (float)wHeight / dims[1];
	} 
	rb_imageResize->SetOutputDimensions( dims[0] * resize, dims[1] * resize, 1);
}

VOID ResizeDicomImage() {
	// Get size of Win32VTKHolderWindow
	LPRECT lpRectMain = (LPRECT)malloc(sizeof(RECT));
	GetWindowRect(hWndVtk, lpRectMain);
	int wWidth = (lpRectMain->right - lpRectMain->left);
	int wHeight = (lpRectMain->bottom - lpRectMain->top);

	int extent[6];
	dicomImageReader->GetDataExtent(extent);
	int dims[] = {
		extent[0] + extent[1],
		extent[2] + extent[3],
		extent[4] + extent[5]
	};

	//Limit to prevent stretching of images
	int dWidth = dims[0] - wWidth;
	int dHeight = dims[1] - wHeight;
	float resize;
	// Find smallest width/height difference and use it as the scaling factor;
	if (dWidth > dHeight) {
		resize = (float)wWidth / dims[0];
	} else {
		resize = (float)wHeight / dims[1];
	}
	// CARE! will result in a float...
	imageResize->SetOutputDimensions( dims[0] * resize, dims[1] * resize, 1);
	// Need to resize the Win32 hWndVtk if I choose to use leftover space...
}

VOID DisplayTopRightMiniText(HDC hdc) { 
	SetTextColor(hdc, RGB(255, 255, 255));
	SetBkColor(hdc, TRANSPARENT);
	TextOut(hdc, 10, 10, TEXT("CORONAL"), strlen("CORONAL")); 
}


VOID DisplayBottomRightMiniText(HDC hdc) { 
	SetTextColor(hdc, RGB(255, 255, 255));
	SetBkColor(hdc, TRANSPARENT);
	TextOut(hdc, 10, 10, TEXT("SAGITTAL"), strlen("SAGITTAL")); 
}


VOID DisplayDicomText(HDC hdc) {
	SetTextColor(hdc, RGB(255, 255, 255));
	SetBkColor(hdc, TRANSPARENT);
	// Get text to be show. 
	char fName[100];
	if (g_singleFileOpen)
		snprintf(fName, 100, "File: %s", dicomImageReader->GetFileName());
	else
		snprintf(fName, 100, "File: %s", dicomImageReader->GetDirectoryName());
	TextOut(hdc, 10, 10, TEXT(fName), strlen(fName)); 

	char uid[200];
	snprintf(uid, 200, "UID: %s", dicomImageReader->GetStudyUID());
	TextOut(hdc, 10, 30, TEXT(uid), strlen(uid)); 

	char name[100];
	snprintf(name, 100, "Patient Name: %s", dicomImageReader->GetPatientName());
	TextOut(hdc, 10, 50, TEXT(name), strlen(name));

	char id[100];
	snprintf(id, 100, "ID: %s", dicomImageReader->GetStudyID());
	TextOut(hdc, 10, 70, TEXT(id), strlen(id)); 

	// 'DICOM' for all tests I have
	char desc[200];
	snprintf(desc, 200, "Description: %s", dicomImageReader->GetDescriptiveName());
	TextOut(hdc, 10, 90, TEXT(desc), strlen(desc)); 

	// Image demensions
	vtkSmartPointer<vtkImageData> imageData = vtkSmartPointer<vtkImageData>::New(); 
	imageData = dicomImageReader->GetOutput();
	int sizeD[3];
	imageData->GetDimensions(sizeD);

	char size[100];
	snprintf(size, 100, "Original: %d x %d x %d", dicomImageReader->GetWidth(), dicomImageReader->GetHeight(), sizeD[2]);
	TextOut(hdc, 10, 110, TEXT(size), strlen(size)); 
}


VOID OpenFileDialog() {
	HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED |
		COINIT_DISABLE_OLE1DDE);
	if (SUCCEEDED(hr)) {
		IFileOpenDialog *pFileOpen; 
		// Create the FileOpenDialog object.
		hr = CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_ALL,
			IID_IFileOpenDialog, reinterpret_cast<void**>(&pFileOpen)); 
		// Sets dialog to FOLDER SELECT
		if (!g_singleFileOpen)
			pFileOpen->SetOptions(FOS_PICKFOLDERS); 
		if (SUCCEEDED(hr)) {
			// Show the Open dialog box.
			hr = pFileOpen->Show(NULL); 
			// Get the file name from the dialog box.
			if (SUCCEEDED(hr)) {
				IShellItem *pItem;
				hr = pFileOpen->GetResult(&pItem); 
				if (SUCCEEDED(hr)) {
					PWSTR pszFilePath;
					hr = pItem->GetDisplayName(SIGDN_FILESYSPATH, &pszFilePath); 
					// Display the file name to the user.
					if (SUCCEEDED(hr)) {
						// need a *char to pass to VTK, converting from Unicode to ACII...
						char buffer[500];
						wcstombs(buffer, pszFilePath, 500);
						OpenDICOMImage(buffer); 
						CoTaskMemFree(pszFilePath);
					}
					pItem->Release();
				}
			}
			pFileOpen->Release();
		}
		CoUninitialize();
	}
}

// May need to invalidata to case a redraw.
VOID DrawLinesForWindow(HWND hWnd, HDC hdc, double hLine, double vLine, int view) { 
	Graphics graphics(hdc); 
	LPRECT lpRect = (LPRECT)malloc(sizeof(RECT));
	GetWindowRect(hWnd, lpRect); 
	Pen rPen(Color(255, 255, 50, 50), 1); 
	Pen gPen(Color(255, 50, 255, 50), 1); 
	Pen bPen(Color(255, 50, 50, 255), 1); 
	int winH, winW;
	int dims[3];

	switch (view) {
	case AXIAL:
		imageResize->GetOutputDimensions(dims);
		//H
		winH = (lpRect->bottom - lpRect->top); 
		graphics.DrawLine(&bPen,
			0,
			dims[1] * hLine + (winH - dims[1]),
			(lpRect->right - lpRect->left),
			dims[1] * hLine + (winH - dims[1]));
		//V
		graphics.DrawLine(&rPen, 
			(lpRect->right - lpRect->left) * vLine, 
			0,
			(lpRect->right - lpRect->left) * vLine, 
			(lpRect->bottom - lpRect->top));
		break;
	case CORONAL:
		rt_imageResize->GetOutputDimensions(dims);
		//H
		winW = (lpRect->right - lpRect->left);
		graphics.DrawLine(&gPen,
			0,
			(lpRect->bottom - lpRect->top) * hLine,
			//(lpRect->right - lpRect->left), 
			dims[0],
			(lpRect->bottom - lpRect->top) * hLine);
		//V
		graphics.DrawLine(&rPen, 
			dims[0] * vLine, 
			0,
			//(lpRect->right - lpRect->left) * vLine, 
			dims[0] * vLine, 
			(lpRect->bottom - lpRect->top));
		break;
	case SAGITTAL:
		rb_imageResize->GetOutputDimensions(dims);
		//H
		winW = (lpRect->right - lpRect->left);
		graphics.DrawLine(&gPen, 
			0, 
			(lpRect->bottom - lpRect->top - (SM_CYMENUSIZE + 6) / 2) * hLine, 
			//(lpRect->right - lpRect->left), 
			dims[0], 
			(lpRect->bottom - lpRect->top - (SM_CYMENUSIZE + 6) / 2) * hLine);
		//V
		graphics.DrawLine(&bPen, 
			dims[0] * (1 - vLine), 
			//(lpRect->right - lpRect->left) * (1 - vLine), 
			0,
			dims[0] * (1 - vLine), 
			//(lpRect->right - lpRect->left) * (1 - vLine), 
			(lpRect->bottom - lpRect->top));
		break;
	default:
		break;
	}
}

VOID UpdateLinePos() {

	int extent[6];
	double spacing[3]; 
	dicomImageReader->GetDataExtent(extent);
	dicomImageReader->GetOutput()->GetSpacing(spacing); 
	g_line[0] = 1 - (g_cSlice / ((extent[1] - extent[0]) * spacing[0]));
	g_line[1] = (g_sSlice / ((extent[3] - extent[2]) * spacing[1]));
	g_line[2] = (g_aSlice / ((extent[5] - extent[4]) * spacing[2]));

	char buffer[100];
	snprintf(buffer, 100, "cS:%f  sS:%f  aS:%f \n", g_line[0], g_line[1], g_line[2]);
	OutputDebugString(buffer);
}